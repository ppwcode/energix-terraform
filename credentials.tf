provider "azurerm" {
  features {}

  subscription_id = "098ad191-f2fd-4240-9a7c-e269ab8394c0"
}

provider "azuread" {
  tenant_id = "6bf21748-a694-4bd1-b1c4-6563b75bde06"
}

terraform {
  required_providers {
    azuread = {
      source  = "hashicorp/azuread"
      version = "=2.18.0"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.98.0"
    }
  }
}
